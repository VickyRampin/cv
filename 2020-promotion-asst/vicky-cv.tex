% Current curriculum vitae, which should include or be accompanied by a record of your professional contributions and activities, a list of committee and other activities with roles played on each group, including committee chair's name, and a description of teaching activities as appropriate.

\documentclass[11pt,a4paper,sans,colorlinks,linkcolor=true]{moderncv}

\moderncvstyle{banking}

\moderncvcolor{burgundy} 

\nopagenumbers{}

% adjust the page margins
\usepackage[scale=0.75]{geometry}

% get nice URLs
\usepackage{url}

% personal data for header
\name{Vicky}{Steeves}
\phone[fixed]{(212)~992~6269}
\email{vicky.steeves@nyu.edu}
\homepage{vickysteeves.com}
\social[twitter]{VickySteeves}
\social[github]{VickySteeves}
\social[gitlab]{VickySteeves}

% remove the bullet between symbols 
\renewcommand*{\makeheaddetailssymbol}{~~~}

% have multiple bibliograpgies
% to get this to work, need to run:
  % latex vicky-cv.tex
  % bibtex pri.aux
  % bibtex sec.aux
  % bibtex tri.aux
  % bibtex quat.aux
  % latex vicky-cv.tex
  % latex vicky-cv.tex

\usepackage{multibib}
\newcites{pri}{Publications}
\newcites{sec}{Presentations \& Posters}
\newcites{tri}{Code \& Media}
\newcites{quat}{Grants Funded}

%----------------------------------------------------------------------------------
%            start CV
%----------------------------------------------------------------------------------
\begin{document}

% set links to be blue
\hypersetup{urlcolor=blue}

% set font size for my name + socials
\renewcommand*{\namefont}{\fontsize{20}{16}\mdseries\upshape}

% make the header with my name + information
\makecvtitle

% lessen the space between title + content
\vspace*{-12mm}

%----------------------------------------------------------------------------------
%            objective
%----------------------------------------------------------------------------------
\section{Professional Objective}
I work to support researchers in creating well-managed, high quality, and reproducible research through integrating reproducible practices into the research workflow. I advocate openness in all facets of scholarship, and enjoy building/contributing to open infrastructure.


%----------------------------------------------------------------------------------
%            more about my research interests
%----------------------------------------------------------------------------------
\section{Research Interests}
\cvitem{}{Data management, reproducibility, digital preservation, digital archiving, systems analysis, database management, web development, software engineering and development, open culture, computational linguistics.}

%----------------------------------------------------------------------------------
%            education
%----------------------------------------------------------------------------------
\section{Education}
\cventry{2019 -- expected 2022}{Master of Computer Science}{New York University}{New York, New York}{}{} 

\cventry{2013 -- 2014}{Master of Library \& Information Science}{Simmons College}{Boston, MA}{}{}

\cventry{2010 -- 2013}{Bachelor of Science Computer Science \& Information Technology}{Simmons College}{Boston, MA}{}{}

\subsection{Graduate and Undergraduate Research Experience}
\cventry{Spring 2014}{Supervisor: Kathy Wisser}{Small World Project}{Boston, MA}{}{Analyzed research done with social media networks on archival material and visualized the analysis using Gephi.}

\cventry{2012 -- 2013}{Supervisor: Nanette Veilleux}{Computational Linguistic Approach to Inflection in Human Speech \& Difference}{Boston, MA}{}{Recorded speech exemplars and used a specialized programming language (\texttt{Praat}) to write a program to determine the tonal center of gravity of each exemplar and make sure that it matches the intended intonational contour.}

\cventry{July 2012}{Celtic Studies}{University College Cork}{Cork, Ireland}{}{Completed coursework in Irish language, literature, and history.}

\subsection{Awards and Honors}
\cventry{2013 -- 2014}{Dean's Fellow Award}{Simmons College}{}{}{Given to students that demonstrate academic excellence.}

\cventry{2013}{The Computer Science Award}{Simmons College Computer Science \& Information Technology Department}{}{}{Given for excellence in Computer Science.}

\cventry{2012 -- 2013}{Collaborative Research Experience for Undergraduates}{Simmons College}{}{}{A research grant given to encourage women in STEM fields to complete research studies in their fields.}

\cventry{2012}{Bowker Award for Travel}{Simmons College}{}{}{Given to undergraduates who demonstrate academic excellence, completing their studies abroad.}

\cventry{2010 -- 2014}{Simmons College 3+1 Undergraduate and Graduate Program}{Simmons College}{}{}{First participant.}

%----------------------------------------------------------------------------------
%            prof experience
%----------------------------------------------------------------------------------
\section{Professional Experience}
\cventry{2018 -- }{Visiting Professor}{Pratt Institute}{New York, NY}{}{I teach data librarianship for the School of Information and Library Science. Materials available here: \url{https://vickysteeves.gitlab.io/lis-628-datalibrarianship/}}

\cventry{2015 -- }{Librarian for Research Data Management \& Reproducibility}{New York University}{New York, NY}{}{As a part of this role, I also am the liaison for data science on campus. In this role, I work to support students, faculty, staff, and researchers in creating well-managed, high quality, and reproducible research through facilitating use of tools such as ReproZip, teaching and instruction, and group and individual consultations.\\}

\-\hspace{1cm}\textit{Outreach:} Organize events to foster a community of data stewardship and reproducibility at NYU (e.g. NYU Reproducibility Symposium), as well as locally in NYC (e.g. METRO workshops), on a national level (e.g. Love Your Data Week), and an international level (e.g. EGU, URFIST). Administered NYU's Carpentries membership and built community around a "train the trainer" model of RDM. Redeveloped and maintained the new Data Services blog, Data Dispatch through piloting a new web hosting service for the Digital Scholarship Services called reClaim. \newline

\-\hspace{1cm}\textit{Service Building:} Built new services around data management and reproducibility in the Libraries. Serve as a consultant to the NYU community on subjects such as: data management, reproducible research practices, data management plans for grants, data visualization, data \& code citation, publishing non-traditional research output, data cleaning, Python, version control, GitHub \& GitLab, data and code licensing, workflow/collaboration tools, and open source tools. Provide multidisciplinary reference services (student hours, email, videoconferencing, phone) to the NYU community. Collaborated to create a new multidepartment service, Data Science and Software Services, with colleagues in IT, the CDS, and PRIISM. Collaborated with Victoria McCoy-Cosentino of NYU’s Compliance office, on inter-service handoffs for Data Use Agreements. \newline

\-\hspace{1cm}\textit{Technical Development:} Test use cases in a development environment for DRSR project, particularly in testing Invenio as a new repository tool for NYU. Coded utilitarian programs for researchers to leverage for data management, including scripts to: bulk rename files, scrape text from RSS feeds, download, unzip, and concatenate many tabular datafiles from NOAA, and create a README for each file. Created and maintain reproduciblescience.org/nyu, a source of information for the NYU community on events, resources, and expertise on campus for reproducibility.\newline

\-\hspace{1cm}\textit{Research:} Won a grant award from the Alfred P. Sloan foundation for the project {Investigating and Archiving the Scholarly Git Experience} (IASGE) to study the way in which scholars learn version control and how GLAMs can best archive software in the Git data format with its scholarly ephemera (e.g. issues, wikis). Successfully completed a grant project funded by the Institute of Museum and Library Services in collaboration with faculty in the Libraries, CDS, and Tandon School of Engineering called {Saving Data Journalism}, which created the first emulated-based archiving tool for complex web applications. Published several papers based on other research on a range of topics, and published the full data and code used in each project for each as well.\newline

\-\hspace{1cm}\textit{Supervisory Roles:} Mentor for ARL FDIE participant in collaboration with Carol Kassel. Supervisor to two Research Scientists for the IASGE project. Day-to-day supervisor for a student consultant in Digital Scholarship Services. Supervised the work of a DS3 Junior Data Scientist on a project to audit the Jupyter ecosystem for accessibility. Co-supervised the first intern for the Research Data Management team in Data Services, with Nick Wolf. Supervised two qualitative and survey student consultants for Data Services.\newline

\cventry{2017 -- 2018}{Adjunct Professor}{Simmons College}{Boston, MA}{}{I taught database management online for the School of Information and Library Science.}

\cventry{2015 -- 2016}{Interim Program Coordinator}{Metropolitan New York Library Council}{New York, NY}{}{I handled the day-to-day operations of METRO's National Digital Stewardship Residency in New York program. I contributed to project planning, communications, documentation, evaluations, outreach, as well as maintained the program's web presence. I planned, organized, and ran NDSR-affiliated events, meetings, and workshops.}

\cventry{2014 -- 2015}{National Digital Stewardship Resident}{American Museum of Natural History}{New York, NY}{}{My project at the AMNH consisted of performing an environmental scan of the Science divisions to better understand their data storage, curation, and preservation needs. After data collection, I identified existing practices and policies for integrated data storage, access, and management. After data analysis, I recommended strategies to digitally preserve the scientific research at the AMNH.}

\cventry{Spring 2014}{Archives Intern}{Sasaki Associates}{Waterville, MA}{}{I processed historical architectural material and wrote the accompanying finding aid and created records for each, then catalogued the collections in Koha ILS}

\cventry{2013 -- 2014}{Dean's Fellow for Technology}{Simmons College}{Boston, MA}{}{Managed social media for the undergraduate science departments to generate interest in STEM at Simmons through outreach to alumnae, current students, and prospective students, working on content creation with faculty.}

%----------------------------------------------------------------------------------
%            instruction
%----------------------------------------------------------------------------------
\section{Teaching \& Instruction}
\subsection{NYU}
\cvitem{Library Classes:}{Taught roughly 30 classes per academic year on research data management and reproducibility best practices and tools across the Division of Libraries.}

\cvitem{Embedded Classes:}{Taught between 25-50 embedded sessions per academic year for faculty on research data management and reproducibility best practices and tools.}

\cvitem{NSF \& NIH Responsible Conduct in Research (RCR)}{Teach federally mandated RCR data management sessions for the Office of Postdoc Affairs, Anthropology Department, Center for Neural Science, and College of Nursing.}

\cvitem{Intensives (half or full day long workshops)}{Advanced Quantitative Reasoning program, December 2016; Steinhardt Food Studies \& Nutrition Summer Workshop, July 2016; Wagner Faculty Workshop, October 2016; Rebecca Amato Humanities Data Workshop, April 2017. Carpentries workshops, March 2018, July 2018, October 2018, January 2019, April 2019, January 2020.}

\cvitem{Carpentries}{Certified instructor. Facilitated NYU membership between Libraries and Center for Data Science. Facilitated Carpentries instructor training for 20 members of the NYU community.}

\cvitem{MCC-UE 14 Media \& Cultural Analysis}{Assisted colleagues in teaching cohort classes in the library, in particular a Media and Communication class.}

\cvitem{{Teaching Coding in Non-coding Classes with Jupyter Notebooks}}{Moderated this panel presentation from educators at Columbia and NYU around the topic of teaching coding with literate programming tools and pedagogies.}

\subsection{National Audience}
\cvitem{National Library of Medicine}{Serve as mentor and instructor for the for-credit continuing education courses: RDM 101 (Spring 2018), RDM 102 (Spring 2019), and RDM 102 (Spring 2020)}

\cvitem{Library Information and Technology Association}{Taught a one-month course on building library services around openness and reproducibility.}

\cvitem{Preservation and Quality Tools}{Facilitated a hands-on session at this workshop on ReproZip.}

\cvitem{Data And Software Preservation for Open Science}{Led multiple hands-on sessions on ReproZip. Presented to the DASPOS board of directors on NYU's open source reproducibility initiatives.}

\cvitem{OpenCon}{Guest lectured on reproducibility for the OpenCon early career librarians call.}

\cvitem{Metropolitan NY Library Council (METRO)}{Presented a webinar teaching RDM principles to library and information professionals.}

\subsection{LIS Programs}
\cvitem{Pratt School of Library and Information Science}{Guest lectured in Meg Smith's Data Librarianship class on my career path, RDM, and reproducibility. 2016 \& 2017}

\cvitem{Palmer School of Library \& Information Science}{Guest lectured in Don Mennerich's Digital Archives class for the Public History program on RDM. 2016 \& 2017}

\cvitem{Queen's College Graduate School of Library \& Information Science}{Guest lectured in Robert DeCandido's digital stewardship class at Queens College on data management, NDSR, and my career path.}

%----------------------------------------------------------------------------------
%            service to the profession
%----------------------------------------------------------------------------------
\section{Service to the Profession}

\subsection{New York University}
\cvitem{The Continuing Contract Faculty Council}{Libraries' first Continuing Contract Faculty Senator, 2015-2017. Committee work includes:}
\begin{itemize}\setlength\itemsep{0.2em}
\item Administration \& Technology Committee (co-chair, subcommittee). Drafted the Policy on University Access to Personal Digital Content and the University Electronic Communications and Social Media Policy.
\item Communications Committee (chair)
\item Faculty Grievance Committee
\item Public Affairs Committee
\item Faculty Committee on the Future of Technology-Enhanced Education. Continuing in this role as of 2019 as a Libraries' representative, no longer a C-FSC representative.
\end{itemize}

% add some space between last bullet and next item
\vspace*{2mm}

\cvitem{Hiring Process}{Participated in the hiring process in several capacities:}
\begin{itemize}\setlength\itemsep{0.2em}
\item Research Data Storage Architect (IT/Division of Libraries)
\item Research Information Scientist (CUSP)
\item Associate Dean for Teaching \& Learning (Libraries)
\item Allied Health Librarian (Libraries)
\item Research Software Engineer (DS3)
\item Data Services Librarian (Libraries
\item Data Services Specialist: Qualitative \& Survey (IT/Libraries)
\item Provostial Postdoc/PostMLIS Resident for DSS (IT/Libraries)
\end{itemize}

 % add some space between last bullet and next item
\vspace*{2mm}

\cvitem{Digital Repository Services for Research}{Collaborating to actively delivering SB2 services, SB3 design (including piloting Invenio and contributing to technical testing), and SB2-to-SB3 integration. User-facing work, including reaching out to pilot users and being available for questions/help. Other work: }
\begin{itemize}\setlength\itemsep{0.2em}
\item Member of the Functional Validation Working Group.
\item Member of the Architecture/Functional Validation Subgroup.
\item Coded a pilot informational website for the RCS project, including UX testing with researchers.
\item Wrote user-facing documentation.
\end{itemize}

% add some space between last bullet and next item
\vspace*{2mm}

\cvitem{Web Archiving Working Group}{Participant.}
\cvitem{Budget Committee}{Participant.}

\subsection{Moore-Sloan Data Science Environment}
\cvitem{Libraries Working Group}{Participate in ongoing collaboration with the two other librarians on the MSDSE, including monthly conference calls and meetups at MSDSE events.}

\cvitem{Reproducibility and Open Science Working Group}{Participate in ongoing collaboration with ROS working groups at the other institutions on the MSDSE, including monthly conference calls and meetups at MSDSE events. Promote a culture of openness and reproducibility on campus through events, workshops, and resource building.}

\subsection{Profession at Large}
\cvitem{Librarians Building Momentum for Reproducibility}{Co-organized a free, half-day, online conference for librarians who are interested in and/or already supporting reproducible research at their institutions: \url{https://vickysteeves.gitlab.io/librarians-reproducibility}}

\cvitem{The LIS Scholarship Archive}{Co-director of \href{https://lissarchive.org/}{LISSA}, a partnership with the Center for Open Science \& the LIS community to build a place for LIS scholars to share their work.}

\cvitem{Software Preservation Network}{One of NYU's institutional members and participant in the technology subgroup.}

\cvitem{SPARC}{Contributor to the SPARC International Survey on Research Data Management Readiness (with Nick Wolf and Scott Collard).}

\cvitem{\href{http://radiandata.org/}{Radian}}{Editor.}

\cvitem{\href{https://theidealis.org/}{The Idealis}}{Editor.}

\cvitem{CLIR}{Participant, 2016 CLIR eResearch Network Research Data Management cohort.}

\cvitem{Preservation and Archiving Special Interest Group}{Chaired the committee responsible for the reproducibility track of the Fall 2016 conference.}

\cvitem{Center for Open Science}{Open Science Framework Ambassador. Participant in the 2016 SHARE Curation Associates program.}

\cvitem{DLF}{Participant in the 2016 eDLF Data Management Cohort.}

%----------------------------------------------------------------------------------
%            bibliographies (1. grants, 2. pubs, 3. pres, 4. code/other)
%----------------------------------------------------------------------------------
\section{2015 -- 2020 Work}

\nocitequat{*}
\bibliographystylequat{vicky}
\bibliographyquat{grants}

\nocitepri{*}
\bibliographystylepri{vicky}
\bibliographypri{publications}

\nocitesec{*}
\bibliographystylesec{vicky}
\bibliographysec{presentations}

\nocitetri{*}
\bibliographystyletri{vicky}
\bibliographytri{media}

\end{document}
